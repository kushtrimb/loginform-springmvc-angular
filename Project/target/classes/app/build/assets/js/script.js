$(document).ready(function(){
	$(".ans").each(function ()
	{
		$(this).click(function () {
			if($(this).hasClass("checked_answer")) {
				$(this).removeClass("checked_answer");
				$(this).attr("data-value", "0");
			}
			else {
				$(this).parent().children().removeClass("checked_answer");
				$(this).parent().children().attr("data-value", "0");
				$(this).addClass("checked_answer");
				$(this).attr("data-value", "1");
			}
		});
	});
	$(".ans_multi").each(function ()
	{
		$(this).click(function () {
			if($(this).hasClass("checked_answer")) {
				$(this).removeClass("checked_answer");
				$(this).attr("data-value", "0");
			}
			else {
				$(this).addClass("checked_answer");
				$(this).attr("data-value", "1");
			}
		});
	});
	$(".answer").each(function ()
	{
		$(this).click(function () {
			if($(this).hasClass("clicked")) {
				$(this).removeClass("clicked");
				$(this).attr("data-value", "0");
			}
			else {
				$(this).parent().children().removeClass("clicked");
				$(this).parent().children().attr("data-value", "0");
				$(this).addClass("clicked");
				$(this).attr("data-value", "1");
			}
		});
	});
});