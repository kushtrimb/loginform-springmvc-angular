angular.module('ngBoilerplate.account', ['ui.router', 'ngResource', 'ngCookies', 'ngRoute'])
.config(function($stateProvider) {
    $stateProvider.state('login', {
        url:'/login',
        views: {
            'main': {
                templateUrl:'account/login.tpl.html',
                controller: 'LoginController'
            }
        },
        data : { pageTitle : "Login" }
}).state('info', {
    url:'/info',
    views: {
        'main': {
            templateUrl:'account/info.tpl.html',
            controller: 'InfoController'
        }
    },
    data : { pageTitle : "Info" }
});
})
.factory('accountService', function($resource,$cookies, $q, $timeout, $http) {
    return {
          myMethod: function(data) {
               return $http.get('/Project/api/auth_user/'+data.name+'/'+data.password+'/')
               .success(function(response) {
               })
               .error(function(response,status) {
                   if(status == 403){
                      alert("Wrong Credentials!");
                   }
               });
              }
           };
})
.controller("LoginCtrl", function($scope,$state,$rootScope,$window,$cookies,accountService) {
    $scope.login = function() {
                  if($scope.account === undefined){
                     alert("Credentials must be provided");
                  }
                  else if($scope.account.name === undefined || $scope.account.name === ""){
                     alert("Username must be provided");
                  }
                  else if($scope.account.password === undefined || $scope.account.password === ""){
                     alert("Password must be provided");
                  }
                  else{
                      accountService.myMethod($scope.account).then(function(result) {
                           $rootScope.account = result.data;
                           $state.go("info");
                      }).then(function(result) {
                      });
                }    
    };    
})

.controller("LoginController", function($scope, $window){
})
.controller("InfoController", function($scope, $state,$rootScope){
	if($rootScope.account === undefined){
		$state.go("login");
	}
	$scope.firstname = $rootScope.account.firstname;
    $scope.lastname = $rootScope.account.lastname;
    $scope.email = $rootScope.account.email; 
});