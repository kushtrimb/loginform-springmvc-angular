angular.module('templates-app', ['account/info.tpl.html', 'account/login.tpl.html']);

angular.module("account/info.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/info.tpl.html",
    "<div class=\"logo\">\n" +
    "            <img src=\"\">\n" +
    "        </div>\n" +
    "<div class=\"heading\">\n" +
    "            <p>Info</p>\n" +
    "</div>\n" +
    "<div class=\"headings\">\n" +
    "            <p>Name : {{firstname}} {{lastname}}</p>\n" +
    "            <p>Email : {{email}}</p>\n" +
    "</div>");
}]);

angular.module("account/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/login.tpl.html",
    "<div class=\"row\" ng-controller=\"LoginCtrl\">\n" +
    "<div class=\"logo\">\n" +
    "            <img src=\"\">\n" +
    "        </div>\n" +
    "<div class=\"heading\">\n" +
    "            <p>Login</p>\n" +
    "        </div>\n" +
    "        <div class=\"inputs\">\n" +
    "            <div class=\"input-username\">\n" +
    "                <img src=\"assets/images/human.png\" />\n" +
    "                <input type=\"text\" placeholder=\"USERNAME\" ng-model=\"account.name\"/>\n" +
    "            </div>\n" +
    "            <div class=\"input-password\">\n" +
    "                <img src=\"assets/images/human.png\" />\n" +
    "                <input type=\"password\" placeholder=\"PASSWORD\" ng-model=\"account.password\"/>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"buttons\">\n" +
    "           <button ng-click=\"login()\"><span><img src=\"assets/images/human.png\" /></span><p>Sign In</p></button>\n" +
    "        </div>\n" +
    "        <div class=\"register-section\">\n" +
    "            <a href=\"#\">Or Register if you don't have account</a>\n" +
    "        </div>\n" +
    " </div>       ");
}]);
