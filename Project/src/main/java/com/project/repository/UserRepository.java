package com.project.repository;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.entity.User;

@Repository
@Transactional
public class UserRepository {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public User getUserByCtredentials(String username, String password){
		Query q = sessionFactory.getCurrentSession().getNamedQuery("User.findByCred")
				.setParameter("username", username)
				.setParameter("password", password);
		
		return (User)q.uniqueResult();
	}
	
	
}
