package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.entity.User;
import com.project.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {
    
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/auth_user/{username}/{password}/", method = RequestMethod.GET)
	public ResponseEntity<User> auth_user(@PathVariable String username,@PathVariable String password) {
		User user = userService.getUserByCredentials(username, password);
		if(user != null){
			return new ResponseEntity<>(user,HttpStatus.OK);
		}
		
		return new ResponseEntity<>(user,HttpStatus.FORBIDDEN);
	}
	
	
}
